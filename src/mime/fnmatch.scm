;; fnmatch C library function via FFI
;;
;;  Copyright (C) 2014  Mike Gerwitz
;;
;;  This file is part of guile-mime.
;;
;;  guile-mime is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License as published
;;  by the Free Software Foundation, either version 3 of the License,
;;  or (at your option) any later version.
;;
;;  This program is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.
;;
;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <http://www.gnu.org/licenses/>.



(define-module (mime fnmatch)
  #:use-module (system foreign)
  #:export (fnmatch))



(define* (fnmatch pattern string
                  #:key (case-fold #f)
                        (allow-error #f))
  "Check whether @var{string} matches the shell wildcard pattern
@var{pattern}.  This is a wrapper around the C function `fnmatch'; see
fnmatch(3) for more information.

The key @var{case-fold} is equivalent to the C flag
@var{FNM_CASEFOLD}: it will perform a case-insensitive match.  The
default behavior is to perform no case folding.

POSIX does not define any conditions under which `fnmatch' may fail,
and glibc guarantees that an error condition will not occur.  The
default behavior of this procedure is therefore to ignore any error
conditions;  if you do not like this, set @var{allow-error}, which
will cause an error to be raised when the underlying `fnmatch` call
fails to return a boolean result."
  (define FNM_NOMATCH 1)
  (define FNM_CASEFOLD (ash 1 4))
  (define %c-fnmatch (pointer->procedure int
                                         (dynamic-func "fnmatch"
                                                       (dynamic-link))
                                         (list '* '* int)))
  (let* ((flags (if case-fold
                    FNM_CASEFOLD
                    0))
         (result (%c-fnmatch (string->pointer pattern)
                             (string->pointer string)
                             flags)))
    (cond
     ((= result 0)
      #t)
     ((or (= result FNM_NOMATCH)
          (eq? #f allow-error))
      #f)
     (else
      (error "fnmatch errno " result)))))
