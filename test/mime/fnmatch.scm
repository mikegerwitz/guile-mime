;; Test case for fnmatch module
;;
;;  Copyright (C) 2014  Mike Gerwitz
;;
;;  This file is part of guile-mime.
;;
;;  guile-mime is free software: you can redistribute it and/or modify
;;  under the terms of the GNU General Public License as published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.
;;
;;  This program is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.
;;
;;  You should have received a copy of the GNU General Public License
;;  along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;; TODO: We should provide stronger assurances, as this does not
;; prevent a bogus implementation.



(define-module (test mime fnmatch)
  #:use-module (srfi srfi-64)
  #:use-module (mime fnmatch))



(test-group
 "(mime fnmatch)"

 (test-group
  "fnmatch procedure"

  (test-eq "returns #t when input matches pattern"
           #t
           (fnmatch "foo*" "foobar"))

  (test-eq "returns #f when input does not match pattern"
           #f
           (fnmatch "foo*" "frobnoz"))

  ;; note that we do not test for errors, because POSIX does not
  ;; define any such conditions, and glibc will never return an error

  (test-group
   "pattern case"

   (test-eq "is not folded by default"
            #f
            (fnmatch "foo" "Foo"))
   (test-eq "is folded with #:case-fold"
            #t
            (fnmatch "foo" "Foo" #:case-fold #t)))))
